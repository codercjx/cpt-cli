const program = require("commander");

const helpOption = () => {
  //增加自己的options
  program.option("-c", "a create project tool cli");

  // <dest> 就是可以接受 -d 后面传过来的路劲了
  /**
   * 比如： -d src/com   会把 'src/com' 放入到program.dest 中
   */
  program.option(
    "-d --dest <dest>",
    "a destination folder, 例如：-d /src/components"
  );

  //监听某个指令
  program.on("--help", function () {
    console.log("");
    console.log("Other: ");
    console.log("   other options~");
  });
};

module.exports = helpOption;
