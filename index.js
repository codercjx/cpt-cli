#!/usr/bin/env node
// 1. 固定写法  #!/usr/bin/env node  在当前电脑环境下找node 可执行文件 并执行当前文件
// #! => 专业叫法叫 shebang/hashbang 是一种指令。
// 作用：当执行一个命令的时候 会根据 #! 后面配置的环境 去执行当前 index.js文件
// 还需要配置运行的指令叫什么名字, 在package.json中配置 bin: { "cpt": "index.js" }  create package tool

// 读取指令的第三发库
const program = require("commander");

const helpOption = require("./lib/core/help.js");
const createCommand = require("./lib/core/create");

//查看版本号 -version -V
program.version(require("./package.json").version);
//可以配置简写，-v 也就是 --version。但是这样子会覆盖program默认的-V,-V就没用了。一般不要修改program默认配置
// program.version(require("./package.json").version, "-v, --version");

// //增加自己的options
// program.option('-c --jx', "a cjx cli");

// // <dest> 就是可以接受 -d 后面传过来的路劲了
// program.option('-d --dest <dest>', "a destination folder, 例如：-d /src/components");

// //监听某个指令
// program.on("--help", function () {
//     console.log("");
//     console.log("Other: ");
//     console.log("   other options");
// })

//帮助可选参数 -h
helpOption();

createCommand();

//解析指令传过来的值。
/**
 * cpt --version: 接收到version 然后在执行上面定义好的 program.version()
 */
program.parse(process.argv);

// 当我们 jx -d /src/view  就把 /src/view 配置到了 program.dest中去了
// console.log(program.dest);
