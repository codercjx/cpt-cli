/**
 * 终端执行命令函数文件
 */

const { spawn, exec } = require("child_process"); //开启另一个进程

const chalk = require("chalk");

/**
 * spawn函数参数说明
 * spawn(command[, args][, options])
 *
 * command <string>要运行的命令
 * args <string[]>字符串参数列表
 * options <Object>
 *      cwd <string>子进程的当前工作目录
 *      ...还有很多，具体看官方文档：https://nodejs.org/dist/latest-v15.x/docs/api/child_process.html#child_process_child_process_spawn_command_args_options
 */

/**
 * 开启进程，执行终端命令
 * @param  {...any} args spawn 函数的参数
 */
const commandeSpawn = (...args) => {
  return new Promise((resolve, reject) => {
    //返回值 childProcess 是进程中执行命令过程的打印信息
    const childProcess = spawn(...args);
    //将childProcess进程中的打印信息放入到当前使用者终端(jx create demo)进程中打印出来
    // childProcess.stdout.pipe(process.stdout);
    // childProcess.stdout.pipe("打印出来了");
    // chalk(process.stdout);
    // console.log(process.stdout);
    childProcess.stderr.pipe(process.stderr); //错误信息也做个输出打印

    //进程中程序执行完毕后，会关闭这个进程，监听关闭进程，然后做相关的业务逻辑
    childProcess.on("close", () => {
      console.log(chalk.rgb(24, 160, 88)("execution completed")); // 绿色
      resolve();
    });
  });
};

module.exports = {
  commandeSpawn,
};
