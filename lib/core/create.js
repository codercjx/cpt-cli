const program = require("commander");

const {
  createProjectAction,
  addVueCpnAction,
  addPageAndRouterAction,
} = require("./actions.js");

//创建指令的函数
const createCommands = () => {
  program
    /**
     * 创建create指令，
     * <project> 接受后面跟的项目名称，
     * [others...] 可以接受<project> 后面的多个参数，
     */
    .command("create <project> [others...]")
    .description("clone a repository into a folder")
    //回调函数，封装
    // .action((project, others) => {
    //     console.log(project, others);
    // })
    .action(createProjectAction);

  program
    .command("addcpn <name>")
    .description(
      "add vue component，例如：jx addcpn NavBar [-d src/components]"
    )
    .action((name) => {
      /**
       * program.dest 表示 命令行中 -d 后面的路径。help.js文件中有介绍
       */
      addVueCpnAction(name, program.dest || "src/components");
    });

  program
    .command("addpage <name>")
    .description("add vue page, 例如：jx addpage home [-d src/components]")
    .action((name) => {
      /**
       * program.dest 表示 命令行中 -d 后面的路径。help.js文件中有介绍
       */
      addPageAndRouterAction(name, program.dest || "src/pages");
    });
};

module.exports = createCommands;
