const path = require("path");

/**
 * 第三方库
 */
const ejs = require("ejs"); 

/**
 * 创建组件/页面/路由/Vuex等等文件模块文件
 * @param {string} templateName 组件/页面的文件名
 * @param {object} data 
 */
const compile = (templateName, data) => {
    // console.log(templateName);
    const templatePath = path.resolve(__dirname, `../templates/${templateName}`);
    return new Promise((resolve, reject) => {
        // resolve(1212);
        // console.log(templatePath);
        ejs.renderFile(templatePath, {data: data}, {}, (err, result) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(result);
        })
    })
}


const writeToFile = (path, content) => {
    /**
     * 补充： 先判断path路劲中的目录是否存在，不存在就创建
     * 比如： /utils/home/koko  先判断 utils是否存在，然后再判断home..... 
     */
    return fs.promises.writeToFile(path, content);
}


const createDir = (path) => {}




module.exports = {
    compile,
    writeToFile
}