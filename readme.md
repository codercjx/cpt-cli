# ctt-cli

#### 介绍

创建前端项目模板,支持 vue2/vue3/react

# ctt-cli

## Table of Contents

- [Install](#install)
- [Usage](#usage)

## Install

```js
  npm install ctt-cli -g
```

## Usage

```js
// 全局安装后
// 查看版本
ctt --version

// create <nmae> : 创建项目目录
ctt create vue-ts
```
