let vueRepo = "direct:https://gitee.com/codercjx/vue-ts-vite.git";

module.exports = {
  vueRepo,
  repoList: [
    {
      url: "direct:https://gitee.com/codercjx/vue2-template.git",
      value: "vue2",
    },
    {
      url: "direct:https://gitee.com/codercjx/vue-ts-vite.git",
      value: "vue3",
    },
  ],
  technicalOptions: [
    {
      name: "action",
      type: "list",
      message: "choice create preject type",
      choices: [
        { name: "vue2", value: "vue2" },
        { name: "vue3", value: "vue3" },
      ],
    },
  ],
};
